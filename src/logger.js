const winston = require("winston");

const logConfig = {
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "logs/info.log"
    }),
    new winston.transports.File({
      level: "error",
      filename: "logs/error.log"
    })
  ]
};
const logger = winston.createLogger(logConfig);

module.exports = logger;
