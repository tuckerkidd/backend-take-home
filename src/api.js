const Pool = require("pg").Pool;
const logger = require("./logger");

const pool = new Pool({
  user: "useradmin",
  host: "localhost",
  database: "notes",
  password: "Test321!",
  port: 5432
});

const getAllNotes = async (request, response) => {
  logger.info(request);
  const limit = request.query.limit ? request.query.limit : "ALL";
  const order = request.query.order ? request.query.order : "DESC";
  const start = request.query.start ? request.query.start : 0;
  pool.query(
    `SELECT * FROM notes ORDER BY id ${order} LIMIT ${limit} OFFSET $1`,
    [start],
    (err, results) => {
      if (err) {
        response.status(500).send(err);
        logger.error(err);
      }
      if (results) {
        response.status(200).json(results.rows);
        logger.info(results.rows);
      }
    }
  );
};

const getNotesById = async (request, response) => {
  logger.info(request);
  const id = request.params.id;
  pool.query("SELECT * FROM notes WHERE id = $1", [id], (err, results) => {
    if (err) {
      response.status(500).send(err);
      logger.error(err);
    }
    if (results) {
      response.status(200).json(results.rows);
      logger.info(results.rows);
    }
  });
};

const addNote = async (request, response) => {
  logger.info(request);
  const { note, type } = request.body;
  pool.query("SELECT * FROM type WHERE type = $1", [type], (error, results) => {
    let typeId = results.rows[0].id;
    let typeName = results.rows[0].type;
    pool.query(
      "INSERT INTO notes (notes, typeId) VALUES ($1, $2)",
      [note, typeId],
      (err, results) => {
        if (err) {
          response.status(500).send(err);
          logger.error(err);
        }
        if (response) {
          response
            .status(201)
            .send(`Note with type ${typeName} added successfully.`);
          logger.info(results.rows);
        }
      }
    );
  });
};

const updateNote = (request, response) => {
  logger.info(request);
  const id = request.params.id;
  const { note } = request.body;
  pool.query(
    "UPDATE notes SET notes = $2 WHERE id = $1",
    [id, note],
    (err, results) => {
      if (err) {
        response.status(500).send(err);
        logger.error(err);
      }
      if (response) {
        response.status(200).send(`Note with id ${id} modified.`);
        logger.info(results.rows);
      }
    }
  );
};

const deleteNote = (request, response) => {
  logger.info(request);
  const id = request.params.id;
  pool.query("DELETE FROM notes WHERE id = $1", [id], (err, results) => {
    if (err) {
      response.status(500).send(err);
      logger.error(err);
    }
    if (response) {
      response.status(200).send(`Note with id ${id} deleted.`);
      logger.info(results.rows);
    }
  });
};

module.exports = {
  getAllNotes,
  getNotesById,
  addNote,
  updateNote,
  deleteNote
};
