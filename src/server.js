const app = require('./index');
const port = 3000;
const api = require('./api');

app.listen(port, (err) => {
    if (err) throw err
    console.log(`Notes app is running on port ${port}.`);
});

app.get('/notes/:id', api.getNotesById);
app.get('/notes', api.getAllNotes);
app.post('/notes/', api.addNote);
app.put('/notes/:id', api.updateNote);
app.delete('/notes/:id', api.deleteNote);